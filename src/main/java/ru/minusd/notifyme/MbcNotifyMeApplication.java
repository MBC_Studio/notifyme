package ru.minusd.notifyme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MbcNotifyMeApplication {

    public static void main(String[] args) {
        SpringApplication.run(MbcNotifyMeApplication.class, args);
    }

}
